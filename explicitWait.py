#!/usr/bin/python

# Importamos las librerias necesarias
from include.Driver import Driver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

def test():
    # Creamos una instancia del driver
    driver = Driver()

    # Obtenemos el driver
    # d = driver.getDriverFirefox(False)
    d = driver.getDriverChrome(False)
    # d = driver.getDriverIE()

    # Obtenemos la siguiente url
    d.get('http://goodstartbooks.com/pruebas/')

    # Esperamos 15 segundos hasta encontrar el elemento
    # d.implicitly_wait(5) # No se necesita en esperas explicitas

    # declaramos una espera explicita
    espera = WebDriverWait(d, 10)
    # Encontramos el boton
    boton = espera.until(EC.element_to_be_clickable((By.ID, "proceed")))
    if boton is not None:
        boton.click()

    time.sleep(3)

    # cerramos el driver
    d.quit()


if __name__ == "__main__":
    test()