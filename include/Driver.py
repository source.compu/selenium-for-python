#!/usr/bin/python

# importamos la libreria
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

#Creamos el driver para trabajar con selenium
class Driver:
    # Constuctor
    def __init__(self):
        # Driver paths
        self.path_chromedriver = './drivers/chromedriver'
        self.path_geckodriver = './drivers/geckodriver'
        self.path_iedriver = './drivers/IEDriverServer'
        # Opciones para Firefox
        self.options = Options()
        self.options.add_argument('--ignore-certificate-error')
        # Opciones para Chrome
        self.chrome_options = webdriver.ChromeOptions()
        self.chrome_options.add_argument('--ignore-certificate-error')
        # Opciones para IE
        self.ie_options = webdriver.IeOptions()
        self.ie_options.add_argument('--ignore-certificate-error')
        # Logs
        self.service_args_chrome = ['--verbose', '--log-path=./logs/chromedriver.log']
        self.service_args_gecko = ['--verbose', '--log-path=./logs/geckodriver.log']
        self.service_args_ie = ['--verbose', '--log-path=./logs/iedriver.log']


    def getDriverFirefox(self, view = True):
        if not view:
            # Ocultamos el navegador
            self.options.headless = True


        driver = webdriver.Firefox(executable_path=self.path_geckodriver, options=self.options)

        return driver


    def getDriverChrome(self, view = True):
        if view:
            # Ocultamos el navegador
            self.chrome_options.headless = True


        # Opciones para no mostrar herramientas de Chrome
        self.chrome_options.add_argument("--no-sandbox") # Corremos como root
        self.chrome_options.add_argument("--no-default-browser-check") #Overrides default choices
        self.chrome_options.add_argument("--no-first-run")
        self.chrome_options.add_argument("--disable-default-apps")
        self.chrome_options.add_argument("--log-level=3")  # fatal # evitamos los mensajes en consola
        # Opcion solo para windows (error corregido)
        # self.chrome_options.add_argument("--disable-gpu")
            
        
        driver = webdriver.Chrome(executable_path=self.path_chromedriver, chrome_options=self.chrome_options)

        return driver


    def getDriverIE(self):

        driver = webdriver.Ie(executable_path=self.path_iedriver, ie_options=self.ie_options)

        return driver
