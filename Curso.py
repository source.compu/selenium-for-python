#!/usr/bin/python

# Importamos las librerias necesarias
from include.Driver import Driver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
import time

def test():
    # Creamos una instancia del driver
    driver = Driver()

    # Obtenemos el driver
    # d = driver.getDriverFirefox(False)
    d = driver.getDriverChrome(False)
    # d = driver.getDriverIE()

    # Obtenemos la siguiente url
    d.get('http://goodstartbooks.com/pruebas/')

    # Esperamos 5 segundos
    d.implicitly_wait(5)

    # Obtenemos el titulo de la pagina
    print("Page Title is : %s" %d.title)

    """
    Regresa un solo elemento a la vez
    """

    elemId = d.find_element_by_id('noImportante')
    if elemId is not None:
        print('Elemento ID encontrado')

    elemName = d.find_element_by_name('ultimo')
    if elemName is not None:
        print('Elemento Name encontrado')

    elemXPath = d.find_element_by_xpath("//tr[@id='noImportante']")
    if elemXPath is not None:
        print('Elemento XPath encontrado')

    elemClass = d.find_element_by_class_name('rojo')
    if elemClass is not None:
        print('Elemento Class encontrado')

    elemLinkText = d.find_element_by_link_text('Pagina 2')
    if elemLinkText is not None:
        print('Elemento Link Text encontrado')

    elemPartialLinkText = d.find_element_by_partial_link_text('agina')
    if elemPartialLinkText is not None:
        print('Elemento Partial Link Text encontrado')

    elemTagName = d.find_element_by_tag_name('h3')
    if elemTagName is not None:
        print('Elemento Tag Name encontrado')


    elemCssSelector = d.find_element_by_css_selector('#primera')
    if elemCssSelector is not None:
        print('Elemento Css Selector encontrado')


    elemFind = d.find_element(By.XPATH, "//tr[@id='noImportante']")
    if elemFind is not None:
        print('Elemento generico encontrado')

    """
    Regresa mas de un elemento a la vez
    """

    elemFinds = d.find_elements(By.TAG_NAME, "tr")
    if elemFind is not None:
        print('Elementos generico encontrado')
    print('se encontraron %d elementos'%len(elemFinds))

    """
    Click en botones de seleccion y casillas de seleccion
    """

    buton1 = d.find_element(By.ID, "CheckboxGroup1_0")
    if buton1 is not None:
        buton1.click()

    time.sleep(2)

    buton2 = d.find_element(By.ID, "CheckboxGroup1_1")
    if buton2 is not None:
        buton2.click()
        
    time.sleep(2)

    buton1 = d.find_element(By.ID, "CheckboxGroup1_0")
    if buton1 is not None:
        buton1.click()

    time.sleep(2)

    buton2 = d.find_element(By.ID, "CheckboxGroup1_1")
    if buton2 is not None:
        buton2.click()

    """
    Seleccionar de un dropdown y una lista de seleccion multiple

    Primero importamos el soporte
    from selenium.webdriver.support.select import Select
    """

    ingredientes = d.find_element(By.NAME, "ingrediente")
    if ingredientes is not None:
        # Obtenemos el select
        ingredientesSel = Select(ingredientes)
        # seleccionamos un item
        ingredientesSel.select_by_value("cebolla")

    time.sleep(2)

    frutas = d.find_element(By.NAME, "Select1")
    if frutas is not None:
        # Obtenemos el select
        frutasSel = Select(frutas)
        # seleccionamos un item
        frutasSel.select_by_index(1)
        frutasSel.select_by_visible_text("Sandia")

    time.sleep(2)

    """
    Obteniendo un atributo o texto
    """

    opcion = d.find_element(By.XPATH, "//*[@id='noImportante']/td[2]")
    if opcion is not None:
        print(opcion.text)

    opcion2 = d.find_element(By.ID, "importante")
    if opcion2 is not None:
        valorAtributo = opcion2.get_attribute("class")
        print("Clase: ", valorAtributo)


    """
    Cambiando el foco a una alerta
    """

    # damos clic al boton para desplegar la alerta
    elem = d.find_element(By.XPATH, "//*[@id='center']/button")
    if elem is not None:
        elem.click()
    time.sleep(3)
    # Hacemos focus a la alerta
    alerta = d.switch_to_alert()
    # Acemos clic en aceptar para cerrar la alerta
    alerta.accept()
    time.sleep(3)


    """
    Cambiando el foco a un frame
    """

    iFrame = d.find_element(By.ID, "pruebas-iframe")
    if iFrame is not None:
        d.switch_to_frame(iFrame)

        # escribimos en el input
        input = d.find_element(By.ID, "Segundo")
        input.send_keys("Paulo")
        time.sleep(3)

        d.switch_to_window(d.current_window_handle)


    """
    Cambiando el foco de la ventana
    """

    # Encontramos la venta actual
    parentHandle  = d.current_window_handle
    print("ParenHandle: ", parentHandle)
    # Encontramos el boton submit y damos clic
    d.find_element(By.ID, "Buton1").click()
    time.sleep(3)
    # encontramos todos los handles y lo comparamos con la pagina actual
    allHandles = d.window_handles
    print("AllHandles: ", allHandles)
    for handle in allHandles:
        if parentHandle != handle:
            d.switch_to_window(handle)
    # escribimos en el input
    input = d.find_element(By.ID, "Segundo")
    input.send_keys("Paulo")
    time.sleep(3)

    """
    Click en botones

    liga = d.find_element(By.XPATH, "//a[contains(text(), 'Pagina 2')]")
    # Validamos que la liga no este vacia
    if liga is not None:
        liga.click() # Hacemos click

    # Estando en la segunda pagina
    input = d.find_element(By.ID, "Segundo")
    # Escribimos
    input.send_keys("Paulo")
    time.sleep(5)
    """

    # cerramos el driver
    d.quit()


if __name__ == "__main__":
    test()