#!/usr/bin/python

# Importamos las librerias necesarias
from include.Driver import Driver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver import ActionChains
import time

def test():
    # Creamos una instancia del driver
    driver = Driver()

    # Obtenemos el driver
    # d = driver.getDriverFirefox(False)
    d = driver.getDriverChrome(False)
    # d = driver.getDriverIE()

    # Obtenemos la siguiente url
    d.get('http://goodstartbooks.com/pruebas/')

    # Esperamos 15 segundos hasta encontrar el elemento
    d.implicitly_wait(15)

    # Encontramos el boton
    botonMenu = d.find_element(By.CLASS_NAME, "dropbtn")
    if botonMenu is not None:
        # Obtenemos las acciones
        acciones = ActionChains(d)
        # acciones
        acciones.move_to_element(botonMenu).perform()
        time.sleep(3)
        liga = d.find_element(By.LINK_TEXT, "Link 1")
        if liga is not None:
            acciones.move_to_element(liga)
            acciones.click()
            acciones.perform()
            

    time.sleep(3)

    # cerramos el driver
    d.quit()


if __name__ == "__main__":
    test()