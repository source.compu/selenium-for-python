#!/usr/bin/python

# Importamos las librerias necesarias
from include.Driver import Driver
import unittest
from selenium.webdriver.common.by import By


# Creamos una clase de prueba
class FindByIdName(unittest.TestCase):
    # Test Fixture (inicio)
    def setUp(self):
        global d
        # Creamos una instancia del driver
        driver = Driver()

        # Obtenemos el driver
        # d = driver.getDriverFirefox(False)
        d = driver.getDriverChrome()
        # d = driver.getDriverIE()


    # Test Fixture (final)
    def tearDown(self):
        # cerramos el driver
        d.quit()


    def testOpen(self):
        # Obtenemos la siguiente url
        d.get('http://goodstartbooks.com/pruebas/')

        # Esperamos 5 segundos
        d.implicitly_wait(5)

        # Obtenemos el titulo de la pagina
        print("Page Title is : %s" %d.title)


    def testId(self):
        # Obtenemos la siguiente url
        d.get('http://goodstartbooks.com/pruebas/')
        # Esperamos 5 segundos
        d.implicitly_wait(5)
        elemId = d.find_element_by_id('noImportante')
        if elemId is not None:
            print('Elemento ID encontrado')


    def testName(self):
        # Obtenemos la siguiente url
        d.get('http://goodstartbooks.com/pruebas/')
        # Esperamos 5 segundos
        d.implicitly_wait(5)
        elemName = d.find_element_by_name('ultimo')
        if elemName is not None:
            print('Elemento Name encontrado')


    def testXPath(self):
        # Obtenemos la siguiente url
        d.get('http://goodstartbooks.com/pruebas/')
        # Esperamos 5 segundos
        d.implicitly_wait(5)
        elemXPath = d.find_element_by_xpath("//tr[@id='noImportante']")
        if elemXPath is not None:
            print('Elemento XPath encontrado')


    def testClass(self):
        # Obtenemos la siguiente url
        d.get('http://goodstartbooks.com/pruebas/')
        # Esperamos 5 segundos
        d.implicitly_wait(5)
        elemClass = d.find_element_by_class_name('rojo')
        if elemClass is not None:
            print('Elemento Class encontrado')


    def testLinkText(self):
        # Obtenemos la siguiente url
        d.get('http://goodstartbooks.com/pruebas/')
        # Esperamos 5 segundos
        d.implicitly_wait(5)
        elemLinkText = d.find_element_by_link_text('Pagina 2')
        if elemLinkText is not None:
            print('Elemento Link Text encontrado')


    def testPartialLinkText(self):
        # Obtenemos la siguiente url
        d.get('http://goodstartbooks.com/pruebas/')
        # Esperamos 5 segundos
        d.implicitly_wait(5)
        elemPartialLinkText = d.find_element_by_partial_link_text('agina')
        if elemPartialLinkText is not None:
            print('Elemento Partial Link Text encontrado')


    def testTagName(self):
        # Obtenemos la siguiente url
        d.get('http://goodstartbooks.com/pruebas/')
        # Esperamos 5 segundos
        d.implicitly_wait(5)
        elemTagName = d.find_element_by_tag_name('h3')
        if elemTagName is not None:
            print('Elemento Tag Name encontrado')


    def testCssSelector(self):
        # Obtenemos la siguiente url
        d.get('http://goodstartbooks.com/pruebas/')
        # Esperamos 5 segundos
        d.implicitly_wait(5)
        elemCssSelector = d.find_element_by_css_selector('#primera')
        if elemCssSelector is not None:
            print('Elemento Css Selector encontrado')


    def testFind(self):
        # Obtenemos la siguiente url
        d.get('http://goodstartbooks.com/pruebas/')
        # Esperamos 5 segundos
        d.implicitly_wait(5)
        elemFind = d.find_element(By.XPATH, "//tr[@id='noImportante']")
        if elemFind is not None:
            print('Elemento generico encontrado')


if __name__ == "__main__":
    unittest.main(warnings='ignore')