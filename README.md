# Selenium with python
Selenium with python

## contenido

Contenido de pruebas para crear un webdriver.

## Como utilizarlo

Es facil solo instancie un objeto de la clase Driver, sintaxis.-

```python
driver = Driver()
```

Para inicizalizar alguno de los web driver, tenemos los siguientes metodos.-

```python
# Firefox
d = getDriverFirefox()
# chrome
d = getDriverChrome()
# IE
d = getDriverIE()
```

> Para que el driver se ejecute en segundo plano, solo hay que pasar el parametro view = True.-

```python
# Firefox
d = getDriverFirefox(True)
# Firefox
d = getDriverFirefox(view=True)
```